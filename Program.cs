﻿using System;

namespace ChessBoardCellColour
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(chessBoardCellColor("C8", "H1"));
        }

        static bool chessBoardCellColor(string cell1, string cell2) {
            if(cell1 == cell2){
                return true;
            }




            bool isDarkColor1 = false;
            bool isDarkColor2 = false;

            char[] c1 = cell1.ToCharArray();
            char[] c2 = cell2.ToCharArray();

            int i1 = Convert.ToInt32(c1[0]);
            int i2 = int.Parse(Convert.ToString(c1[1]));

            int i3 = Convert.ToInt32(c2[0]);
            int i4 = int.Parse(Convert.ToString(c2[1]));

            if((i1 % 2 == 0 && i2 % 2 == 0) || (i1 % 2 != 0 && i2 % 2 != 0))   {
                isDarkColor1 = true; //dark color
            }

            if((i3 % 2 == 0 && i4 % 2 == 0) || (i3 % 2 != 0 && i4 % 2 != 0)){
                isDarkColor2 = true; // dark color
            }

            // light orange = x & y == (Odd || Even)
            // dark orange = (x = Odd && y == Even) || (x = Even && y == Odd)

            // Given  cell1 and cell2
            // Check what th x and y is for cell 1 to determine the color
            // Check  what the x and y is for cell 2  
            // See if they are the same

            // C8 and H1
            // C = 3, H = 8
            // cell 1 is light color
            // cell 2 is light color
            // return true 

            // Both are the dark color oherwise false
            return isDarkColor1 == isDarkColor2;
            
        }
    }
}